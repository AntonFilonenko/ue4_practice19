﻿#include <iostream>
using namespace std;
class Animal {
protected:
    string name;
public:
    virtual void Voice()
    {
        cout << "who am I? uuuu \n" << "My name is " << name << "\n\n";
    }
    void SetNewName(string _name)
    {
        cout << "My old name was " << name << "\n";
        name = _name;
        cout << "My new name is " << name << "\n\n";
    }
    Animal() {};
    Animal(string _name)
    {
        name = _name;
    }
};

class Dog :public Animal {
public:
    void Voice() override
    {
        cout << "I am dog \n" << "My name is " << name << "\n\n";
    }
    Dog(string _name)
    {
        name = _name;
    }
};

class Cat :public Animal {
public:
    void Voice() override
    {
        cout << "I am cat \n" << "My name is " << name << "\n\n";
    }
    Cat(string _name)
    {
        name = _name;
    }
};

class Cow :public Animal {
public:
    void Voice() override
    {
        cout << "I am dog \n" << "My name is " << name << "\n\n";
    }
    Cow(string _name)
    {
        name = _name;
    }
};
int main()
{
    Animal* animals[4];

    animals[0] = new Dog("Charly");
    animals[1] = new Cat("Barsik");
    animals[2] = new Cow("Mumu");
    animals[3] = new Animal("Thing");

    for (int i = 0; i < 4; i++)
    {
        animals[i]->Voice();
    }
    
}
